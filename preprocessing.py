#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import numpy as np

import importlib

path = os.path.abspath(os.path.dirname(__file__))
if not path in sys.path:
    sys.path.insert(1, path)
del path
import model
importlib.reload(model)

# ============================================================================
# Local solver interface for preprocessing
# ============================================================================

class SolverWrapper(object):
    """docstring for SolverWrapper"""
    def __init__(self):
        super(SolverWrapper, self).__init__()

    def addNeuron(self, neuron, **args):
        raise NotImplementedError('Abstract class: use a concrete solver API')

    def boundsForNeuron(self, neuron, **args):
        raise NotImplementedError('Abstract class: use a concrete solver API')
        


# ============================================================================
# Fast bounds via Interval Based Reasoning
# ============================================================================

def ibr_bounds(net):
    # Sequentially process layers
    for lidx, layer in enumerate(net.layers()):
        if issubclass(layer.__class__, model.DNRActLayer):
            # Compute neuron activation
            for nrn in layer.neurons():
                # Compute the neuron activity
                yub, ylb = nrn.bias(), nrn.bias()
                for idx, wgt in zip(nrn.connected(), nrn.weights()):
                    prd = net.neuron(idx)
                    if wgt >= 0:
                        yub += wgt * prd.ub()
                        ylb += wgt * prd.lb()
                    else:
                        yub += wgt * prd.lb()
                        ylb += wgt * prd.ub()
                # Apply the activation function
                lb = model.act_eval(nrn.activation(), ylb)
                ub = model.act_eval(nrn.activation(), yub)
                # Update the bounds
                nrn.update_ylb(ylb)
                nrn.update_yub(yub)
                nrn.update_lb(lb)
                nrn.update_ub(ub)

# ============================================================================
# Forward bound tightening via Mixed Integer Linear Programming
# ============================================================================

def fwd_bound_tighthening(net, slv, timelimit=None, skip_layers=None,
        verbose=0):
    # Process the network layer by layer
    ttime, nleft = 0, net.size()
    for layer in net.layers():
        # Add the layer to the solver wrapper
        for neuron in layer.neurons():
            # Add the neuron to the model
            if verbose >= 1:
                print('Adding neuron %s' % str(neuron.idx()))
            slv.addNeuron(neuron)
            # Do not compute the bounds for skipped layers
            if skip_layers is not None and layer.idx() in skip_layers:
                continue
            # Obtain a time limit for computing bounds
            if timelimit is not None:
                tlim = (timelimit - ttime) / nleft
            else:
                tlim = None
            # Compute bounds
            if verbose >= 1:
                print('Computing bounds for %s' % str(neuron.idx()))
            ltime, bchg = slv.boundsForNeuron(neuron, timelimit=tlim,
                    verbose=verbose)
            ttime += ltime
            nleft -= 1
    # Return total time
    return ttime

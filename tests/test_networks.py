#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import numpy as np

import importlib

# Import custom modules
path = os.path.join(os.path.dirname(__file__), '..')
path = os.path.abspath(path)
if not path in sys.path:
    sys.path.insert(1, path)
del path

import model
importlib.reload(model)

def gen_wgt_bias(nin, nout, ntype, k):
    if ntype == 'cancel':
        wgt = np.ones((nin, nout))
        if k % 2 == 0:
            diag = np.arange(min(nin, nout))
            wgt[diag, diag] = -1
        bias = np.zeros(nout)
    elif ntype == 'ridge':
        wgt = np.ones((nin, nout))
        if k % 2 == 0:
            diag = np.arange(min(nin, nout)-1)
            wgt[1+diag, diag] = -1
        bias = np.zeros(nout)
    elif ntype == 'sum':
        wgt = np.ones((nin, nout))
        bias = np.zeros(nout)
    else:
        raise ValueError('Unsupported network type "%s"' % ntype)
    print(wgt)
    return wgt, bias

def gen_net(nhidden, width, ntype='inverting'):
    # Build a DNR network model
    net = model.DNRNet()
    # ------------------------------------------------------------------------
    # Add input layer
    # ------------------------------------------------------------------------
    layer = model.DNRInput(input_shape=(width,), lb=0, ub=1)
    net.add(layer)
    # ------------------------------------------------------------------------
    # Add hidden layers
    # ------------------------------------------------------------------------
    for k in range(nhidden):
        wgt, bias = gen_wgt_bias(width, width, ntype, k)
        layer = model.DNRDense(wgt, bias, activation='linear')
        net.add(layer)
    # ------------------------------------------------------------------------
    # Add output layer
    # ------------------------------------------------------------------------
    wgt, bias = gen_wgt_bias(width, 1, ntype, nhidden)
    layer = model.DNRDense(wgt, bias, activation='linear')
    net.add(layer)
    # Return the network
    return net

if __name__ == '__main__':
    # Load and print a few inverting networks
    print(load_inverting_net(3, 2))

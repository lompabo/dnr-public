#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import numpy as np
import keras.models as kmodels
import keras.layers as klayers
import pickle

import importlib

# Import custom modules
path = os.path.join(os.path.dirname(__file__), '..', '..')
path = os.path.abspath(path)
if not path in sys.path:
    sys.path.insert(1, path)
del path

import model
importlib.reload(model)
from readers import kerasreader
importlib.reload(kerasreader)


if __name__ == '__main__':
    # Build a random training set
    ns, na, nh, no = 200, 3, 2, 1
    epochs = 100
    np.random.seed(100)
    datax = np.random.random((ns, na))
    datay = np.sum(datax, axis=1)
    # Learn a super-simple keras model
    mdl = kmodels.Sequential()
    mdl.add(klayers.Dense(nh, input_shape=(na,), activation='relu'))
    mdl.add(klayers.Dense(nh, activation='relu'))
    mdl.add(klayers.Dense(no))
    mdl.compile(optimizer='adam', loss='mse')
    mdl.fit(datax, datay, epochs=epochs)

    # Convert the network in DNR format
    net = kerasreader.read_keras_sequential(mdl)

    # for nrn in net.layer(1).neurons():
    #     for idx, wgt in zip(nrn.connected(), nrn.weights()):
    #         prd = net.neuron(idx)

    # Obtain keras predictions
    kx = np.random.random((10, na))
    ky = mdl.predict(kx)
    # Obtain DNR net predictions
    for i in range(kx.shape[0]):
        dnr_eval = net.evaluate(kx[i])
        diffs = np.abs(dnr_eval.layer(-1) - ky[i])
        assert(np.all(diffs < 1e-4))

    # Save the network via pickle
    print('Writing network to file')
    dnnfile = os.path.join(os.path.dirname(__file__), '..', 'dbgnet.dat')
    with open(dnnfile, 'wb') as fp:
        pickle.dump(net, fp, protocol=2)


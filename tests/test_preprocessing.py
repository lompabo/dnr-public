#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import numpy as np
import pickle

import importlib

# Import custom modules
path = os.path.join(os.path.dirname(__file__), '..')
path = os.path.abspath(path)
if not path in sys.path:
    sys.path.insert(1, path)
del path

import model
importlib.reload(model)
import preprocessing
importlib.reload(preprocessing)
from solverapi import cplexapi
importlib.reload(cplexapi)
import test_networks
importlib.reload(test_networks)


if __name__ == '__main__':
    # Load an inverting network
    nhidden, width = 3, 3 
    dnn = test_networks.gen_net(nhidden, width, 'ridge')
    print('=== Original network')
    print(dnn)

    # Run IRB bound computation
    print('=== Running IBR bound tightening')
    preprocessing.ibr_bounds(dnn)
    print(dnn)

    # Start forward bound tightening
    print('=== Running forward bound tightening with Cplex')
    slv = cplexapi.CplexWrapper()
    preprocessing.fwd_bound_tighthening(dnn, slv)
    print(slv)
    print(dnn)



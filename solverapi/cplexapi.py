#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import importlib

import sys

import docplex.mp.model as cpx

# Import custom modules
path = os.path.join(os.path.dirname(__file__), '..')
path = os.path.abspath(path)
if not path in sys.path:
    sys.path.insert(1, path)
del path

import model
importlib.reload(model)

import preprocessing
importlib.reload(preprocessing)

class CplexWrapper(preprocessing.SolverWrapper):
    """docstring for SolverWrapper"""
    def __init__(self):
        super(CplexWrapper, self).__init__()
        self.mdl_ = cpx.Model()
        self.exps_ = {}

    def xAdd(self, xtype, xidx, val):
        self.exps_[(xtype,) + xidx] = val

    def xGet(self, xtype, xidx):
        return self.exps_[(xtype,) + xidx]

    def xHas(self, xtype, xidx):
        return ((xtype,) + xidx) in self.exps_

    def addNeuron(self, neuron, **args):
        # Cache some attribute
        mdl = self.mdl_
        # Obtain cplex-friendly bounds
        lb, ub = neuron.lb(), neuron.ub()
        lb = lb if lb != -float('inf') else -mdl.infinity()
        ub = ub if ub != float('inf') else mdl.infinity()
        # --------------------------------------------------------------------
        # Build a variable for the model output
        # --------------------------------------------------------------------
        idx = neuron.idx()
        x = mdl.continuous_var(lb=lb, ub=ub, name='x%s' % str(idx))
        self.xAdd('x', idx, x)
        # --------------------------------------------------------------------
        # Check whether this is a neuron with an activation function
        # --------------------------------------------------------------------
        net = neuron.network()
        if issubclass(neuron.__class__, model.DNRActNeuron):
            # Build an expression for the neuron activation
            yterms = [neuron.bias()]
            for pidx, wgt in zip(neuron.connected(), neuron.weights()):
                prdx = self.xGet('x', pidx)
                yterms.append(prdx * wgt)
            y = mdl.sum(yterms)
            self.xAdd('y', idx, y)
            # TODO add the redundant constraints by Sanner
            # TODO add bounding constraints on the y expression
            # ----------------------------------------------------------------
            # Introduce the csts and vars for the activation function
            # ----------------------------------------------------------------
            act = neuron.activation()
            if act == 'relu':
                ylb, yub = neuron.ylb(), neuron.yub()
                # Trivial case 1: the neuron is always active
                if ylb >= 0:
                    mdl.add_constraint(x == y)
                # Trivial case 1: the neuron is always inactive
                elif yub <= 0:
                    mdl.add_constraint(x == 0)
                # Handle the non-trivial case
                else:
                    # Enfore the natural bound on the neuron output
                    # NOTE if interval based reasoning has been used to
                    # compute bounds, this will be always redundant
                    x.lb = max(0, lb)
                    # Introduce a binary activation variable
                    z = mdl.binary_var(name='z%s' % str(idx))
                    self.xAdd('z', idx, z)
                    # Introduce a slack variable
                    s = mdl.continuous_var(ub=-ylb, name='s%s' % str(idx))
                    self.xAdd('s', idx, s)
                    # Buid main constraint
                    cst = mdl.add_constraint(x - s == y, ctname='r0%s' % str(idx))
                    # Build indicator constraints
                    mdl.add_indicator(z, s == 0, 1, name='r1%s' % str(idx))
                    mdl.add_indicator(z, x == 0, 0, name='r2%s' % str(idx))
            elif act == 'linear':
                mdl.add_constraint(x == y)
            else:
                raise ValueError('Unsupported "%s" activation function' % act)


    def boundsForNeuron(self, neuron, timelimit=None, verbose=0, **args):
        # Prepare some data structures
        idx, mdl, net = neuron.idx(), self.mdl_, neuron.network()
        ttime, bchg = 0, False
        if issubclass(neuron.__class__, model.DNRActNeuron):
            act = neuron.activation()
        else:
            act = None
        # --------------------------------------------------------------------
        # Compute an upper bound
        # --------------------------------------------------------------------
        # Choose the correct objective function
        mdl.set_objective('max', self.xGet('x', idx))
        # Solve the problem and extract the best bound
        res = mdl.solve()
        # Extract the bound
        if res.solve_details.status == 'optimal':
            ub = res.objective_value
        else:
            ub = mdl.solve_details.best_bound
        ttime += mdl.solve_details.time
        # Enforce the bound
        # NOTE for activation neurons, this is an _activation_ bound!
        if act is not None:
            neuron.update_yub(ub)
            neuron.update_ub(model.act_eval(act, ub))
        else:
            neuron.update_ub(ub)
        # --------------------------------------------------------------------
        # Compute a lower bound
        # --------------------------------------------------------------------
        # Choose the correct objective function
        if act is not None:
            # ReLU activation function
            if act == 'relu' and self.xHas('s', idx):
                mdl.set_objective('min', -self.xGet('s', idx))
            elif act == 'linear':
                mdl.set_objective('min', self.xGet('x', idx))
        else:
            mdl.set_objective('min', self.xGet('x', idx))
        # Solve the problem and extract the best bound
        res = mdl.solve()
        # Extract the bound
        if res.solve_details.status == 'optimal':
            lb = res.objective_value
        else:
            lb = mdl.solve_details.best_bound
        ttime += mdl.solve_details.time
        # Enforce the bound
        # NOTE for activation neurons, this is an _activation_ bound!
        if issubclass(neuron.__class__, model.DNRActNeuron):
            neuron.update_ylb(lb)
            neuron.update_lb(model.act_eval(act, lb))
        else:
            neuron.update_lb(lb)
        # --------------------------------------------------------------------
        # Return results
        # --------------------------------------------------------------------
        return ttime, bchg

    def __del__(self):
        self.mdl_.end()

    def __enter__(self):
        return self

    def __exit(self):
        mdl.end()

    def __repr__(self):
        mdl = self.mdl_
        s = ''
        # Print objective
        if mdl.is_minimize():
            s += 'minimize: %s\n' % mdl.get_objective_expr()
        else:
            s += 'maximize: %s\n' % mdl.get_objective_expr()
        # Print all constraints
        s += 'subject to:\n'
        for cst in self.mdl_.iter_constraints():
            s += '\t%s\n' % str(cst)
        # Print all variables
        s += 'with vars:\n'
        for var in self.mdl_.iter_variables():
            s += '\t%f <= %s <= %f\n' % (var.lb, str(var), var.ub)
        return s







